## potato_RMX1851-eng 12 SP1A.210812.016 f026bd4708 test-keys
- Manufacturer: realme
- Platform: sdm710
- Codename: RMX1851
- Brand: Realme
- Flavor: potato_RMX1851-eng
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: f026bd4708
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Realme/potato_RMX1851/RMX1851:12/SP1A.210812.016/ba7689179e:eng/test-keys
- OTA version: 
- Branch: potato_RMX1851-eng-12-SP1A.210812.016-f026bd4708-test-keys
- Repo: realme_rmx1851_dump_1172


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
